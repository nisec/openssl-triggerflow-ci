This repository continuosly tests every new version of "OpenSSL":https://www.openssl.org/ for vulnerability to side-channel attacks, checking execution paths against known unsafe points.

Powered by "triggerflow":https://gitlab.com/Voker57/triggerflow and "repatcher":https://gitlab.com/Voker57/repatcher

h2. Credits

h3. Authors

* Iaroslav Gridin (Tampere University, Tampere, Finland)
* Billy Bob Brumley (Tampere University, Tampere, Finland)
* Sohaib ul Hassan (Tampere University, Tampere, Finland)
* Cesar Pereida García (Tampere University, Tampere, Finland)
* Nicola Tuveri (Tampere University, Tampere, Finland)

h3. Funding

This project has received funding from the European Research Council (ERC) under the European Union's Horizon 2020 research and innovation programme (grant agreement No 804476).
